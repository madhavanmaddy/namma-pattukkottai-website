---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Namma Pattukkottai"
  text: "We are digitizing our City"

features:
  - title: Reach your Customers
    details: Advertise your business and reach your target Audience.
  - title: Targetted Notifications
    details: Send targetted Notifications to your customers.
  - title: Recruit your Employees
    details: Post the Job Oppurtunities in your businesses.
---