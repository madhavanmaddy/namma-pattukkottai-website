---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Namma Pattukkottai"
  text: "We are digitizing our City"

features:
  - title: Know the Businesses
    details: Get the details of the businesses in and around our city.
  - title: See what's Playing
    details: Get the details of movies playing in theatres near you.
  - title: City is filled with Oppurtunities
    details: Get the details of Jobs oppourtunities in our city.
  - title: Properties
    details: Get the details of Properties available for Sale and Rent.
  - title: Get Notified
    details: Get Notified about all the latest news and updates in our city.
  - title: Support for Tamil and English
    details: Know our city in your preferred language.
---

