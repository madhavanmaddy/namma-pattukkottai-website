import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Namma Pattukkottai",
  description: "We are digitizing Namma Pattukkottai",
  themeConfig: {
    nav: [
      { text: 'For Individuals', link: '/' },
      { text: 'For Businesses', link: '/business' }
    ],
  },
  outDir: "../public"
})
